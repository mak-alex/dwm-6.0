#!/bin/bash
source=(http://dl.suckless.org/dwm/dwm-$pkgver.tar.gz
	dwm.desktop
	config.h
  bstack.c
  gaplessgrid.c
  push.c)
_patches=(01-statuscolours.diff
          02-monoclecount.diff
          03-noborder.diff
          04-centredfloating.diff
          05-scratchpad.diff
          06-attachaside.diff
          07-nopaddedbar.diff)
source=(${source[@]} ${_patches[@]})

build() {
  #cd $pkgname-$pkgver


  for p in "${_patches[@]}"; do
    echo "patch => $p"
    patch < $p || return 1
  done
}

package() {
  cd $pkgname-$pkgver
  make PREFIX=/usr DESTDIR=$pkgdir install
  install -m644 -D LICENSE $pkgdir/usr/share/licenses/$pkgname/LICENSE
  install -m644 -D README $pkgdir/usr/share/doc/$pkgname/README
  install -m644 -D dwm.desktop $pkgdir/usr/share/xsessions/dwm.desktop
}
build