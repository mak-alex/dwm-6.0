/* See LICENSE file for copyright and license details. */
/* Author: Alex M.A.K. (a.K.a) FlashHacker
 * Date: 09-09-2014
 * Name: config.h
 */
/* appearance */
static const char font[] = "-*-xbmicons-medium-r-*-*-13-*-*-*-*-*-*-*" ","
                           "-*-ohsnap-medium-r-normal-*-13-*-*-*-*-*-*-*";
#define NUMCOLORS 12
static const char colors[NUMCOLORS][ColLast][9] = {
  // border foreground background
  { "#282a2e", "#373b41", "#1d1f21" }, // 1 = normal (grey on black)
  { "#f0c674", "#c5c8c6", "#1d1f21" }, // 2 = selected (white on black)
  { "#dc322f", "#1d1f21", "#f0c674" }, // 3 = urgent (black on yellow)
  { "#282a2e", "#282a2e", "#1d1f21" }, // 4 = darkgrey on black (for glyphs)
  { "#282a2e", "#1d1f21", "#282a2e" }, // 5 = black on darkgrey (for glyphs)
  { "#282a2e", "#cc6666", "#1d1f21" }, // 6 = red on black
  { "#282a2e", "#b5bd68", "#1d1f21" }, // 7 = green on black
  { "#282a2e", "#de935f", "#1d1f21" }, // 8 = orange on black
  { "#282a2e", "#f0c674", "#282a2e" }, // 9 = yellow on darkgrey
  { "#282a2e", "#81a2be", "#282a2e" }, // A = blue on darkgrey
  { "#282a2e", "#b294bb", "#282a2e" }, // B = magenta on darkgrey
  { "#282a2e", "#8abeb7", "#282a2e" }, // C = cyan on darkgrey
};
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 8;        /* snap pixel */
static const Bool showbar           = True;     /* False means no bar */
static const Bool topbar            = True;     /* False means bottom bar */

/* tagging */
static const char *tags[] = { "term \uE019", "inet\uE019", "dev\uE019", 
"files\uE019", "im\uE019", "office\uE019", "media\uE019", "games\uE019","other\uE019" };

static const Rule rules[] = {
    /* class                     instance    title      tags mask       isfloating      iscentred   monitor */  
    { "URxvt",                    NULL,       NULL,       1 << 0,         False,          False,          -1 },
    { "Tmux",                     NULL,       NULL,       1 << 0,         False,          False,          -1 },
    { "LXTerminal",               NULL,       NULL,       1 << 0,         False,          False,          -1 },
    { "Xterm",                    NULL,       NULL,       1 << 0,         False,          False,          -1 },
    { "Gnome-terminal",           NULL,       NULL,       1 << 0,         False,          False,          -1 },
    { "Konsole",                  NULL,       NULL,       1 << 0,         False,          False,          -1 },
    // inet
    { "Iceweasel",                NULL,       NULL,       1 << 1,         False,          False,          -1 },
    { "Firefox",                  NULL,       NULL,       1 << 1,         False,          False,          -1 },
    { "Google-chrome",            NULL,       NULL,       1 << 1,         False,          False,          -1 },
    { "Dwb",                      NULL,       NULL,       1 << 1,         False,          False,          -1 },
    { "Chromium",                 NULL,       NULL,       1 << 1,         False,          False,          -1 },
    // develop/edit
    { "Sublime Text",             NULL,       NULL,       1 << 2,         False,          False,          -1 },
    { "Sublime",                  NULL,       NULL,       1 << 2,         False,          False,          -1 },
    { "sublime",                  NULL,       NULL,       1 << 2,         False,          False,          -1 },
    { "Geany",                    NULL,       NULL,       1 << 2,         False,          False,          -1 },
    { "Medit",                    NULL,       NULL,       1 << 2,         False,          False,          -1 },
    { "Gvim",                     NULL,       NULL,       1 << 2,         False,          False,          -1 },
    { "Monodevelop",              NULL,       NULL,       1 << 2,         False,          False,          -1 },
    { "Emacs",                    NULL,       NULL,       1 << 2,         False,          False,          -1 },
    // files
    { "Thunar",                   NULL,       NULL,       1 << 3,         False,          False,          -1 },
    { "Spacefm",                  NULL,       NULL,       1 << 3,         False,          False,          -1 },
    { "Pcmanfm",                  NULL,       NULL,       1 << 3,         False,          False,          -1 },
    // im/mail
    { "Pidgin",                   NULL,       NULL,       1 << 4,         False,          False,          -1 },
    { "Wheechat",                 NULL,       NULL,       1 << 4,         False,          False,          -1 },
    { "Skype",                    NULL,       NULL,       1 << 4,         False,          False,          -1 },
    // office
    { "Libreoffice",              NULL,       NULL,       1 << 5,         False,          False,          -1 },
    { "Abiword",                  NULL,       NULL,       1 << 5,         False,          False,          -1 },
    { "Xpdf",                     NULL,       NULL,       1 << 5,         False,          False,          -1 },
    { "Evince",                   NULL,       NULL,       1 << 5,         False,          False,          -1 },
    { "libreoffice-calc",         NULL,       NULL,       1 << 5,         False,          False,          -1 },
    { "libreoffice-impress",      NULL,       NULL,       1 << 5,         False,          False,          -1 },
    { "libreoffice-startcenter",  NULL,       NULL,       1 << 5,         False,          False,          -1 },
    { "libreoffice-writer",       NULL,       NULL,       1 << 5,         False,          False,          -1 },
    // graph/media
    { "Inkscape",                 NULL,       NULL,       1 << 6,         False,          False,          -1 },
    { "Gimp",                     NULL,       NULL,       1 << 6,         False,          False,          -1 },
    { "Feh",                      NULL,       NULL,       1 << 6,         False,          False,          -1 },
    { "Sxiv",                     NULL,       NULL,       1 << 6,         False,          False,          -1 },
    { "Mplayer",                  NULL,       NULL,       1 << 6,         False,          False,          -1 },
    { "SMPlayer",                 NULL,       NULL,       1 << 6,         False,          False,          -1 },
    { "Vlc",                      NULL,       NULL,       1 << 6,         False,          False,          -1 },
    { "Audacious",                NULL,       NULL,       1 << 6,         False,          False,          -1 },
    { "Clementine",               NULL,       NULL,       1 << 6,         False,          False,          -1 },
    // games
    { "Warmux",                   NULL,       NULL,       1 << 7,         False,          False,          -1 },
    { "Warzone2100",              NULL,       NULL,       1 << 7,         False,          False,          -1 },
    { "Freeciv",                  NULL,       NULL,       1 << 7,         False,          False,          -1 },
    // other
    { "Qbittorrent",              NULL,       NULL,       1 << 8,         False,          False,          -1 },
    { "Transmission-gtk",         NULL,       NULL,       1 << 8,         False,          False,          -1 },
    { "Deluge",                   NULL,       NULL,       1 << 8,         False,          False,          -1 },
    { "Rtorrent",                 NULL,       NULL,       1 << 8,         False,          False,          -1 },
    { "VirtualBox",               NULL,       NULL,       1 << 8,         False,          False,          -1 },
    { "Xephyr",                   NULL,       NULL,       1 << 8,         False,          False,          -1 },
    { "Qemu",                     NULL,       NULL,       1 << 8,         False,          False,          -1 },
};

/* layout(s) */
static const float mfact      = 0.50;  /* factor of master area size [0.05..0.95] */
static const int nmaster      = 1;     /* number of clients in master area */
static const Bool resizehints = False; /* True means respect size hints in tiled resizals */

#include "bstack.c"
#include "gaplessgrid.c"
static const Layout layouts[] = {
    /* symbol     arrange function */
    { "\uE019 \uE009 \uE019",    tile },    /* first entry is default */
    { "\uE019 \uE00A \uE019",    NULL },    /* no layout function means floating behavior */
    { "\uE019 \uE00B \uE019",    monocle },
    { "\uE019 \uE00C \uE019",    bstack },
    { "\uE019 \uE00D \uE019",    gaplessgrid },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *menu[] = { "dmenu_run", "-i", "-fn", font, "-nb", colors[0][ColBG], "-nf", colors[0][ColFG], "-sb", colors[1][ColBG], "-sf", colors[9][ColFG], NULL };
static const char *dmenucmd[] = { "dmenu_extended_run", "-m", dmenumon, "-fn", font, "-nb", colors[0][ColBG], "-nf", colors[0][ColFG], "-sb", colors[1][ColBG], "-sf", colors[1][ColFG], NULL };
static const char *menu9cmd[]  = { "9menu", "-popup", "-teleport", "-file", "/home/flashhacker/.9menurc", "-bg", "#121313", "-fg", "#A3B2A8", "-font", "-*-ohsnap-medium-r-normal-*-14-*-*-*-*-*-*-*", NULL };
static const char *menucmd[]  = { "bash /home/flashhacker/9menu-full-script", NULL };
static const char *webb[] = { "iceweasel", NULL, "Iceweasel" };
static const char *chat[] = { "skype", NULL, "Skype" };
static const char *termcmd[] = { "urxvtcd", NULL, "URxvt" };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "urxvtc", "-name", scratchpadname, "-geometry", "100x25", NULL };
static const char *edit[] = { "sublime", NULL, "Geany" };
static const char *mdia[] = { "vlc", NULL, "Vlc" };
static const char *file[] = { "thunar", NULL, "Thunar" };
static const char *prts[] = { "scrot", NULL };
static const char *play[] = { "ncmpcpp", "toggle" };
static const char *stop[] = { "ncmpcpp", "stop" };
static const char *prev[] = { "ncmpcpp", "prev" };
static const char *next[] = { "ncmpcpp", "next" };
static const char *mute[] = { "amixer", "-q", "set", "PCM", "toggle", NULL };
static const char *volu[] = { "amixer", "-q", "set", "PCM", "5%+", "unmute", NULL };
static const char *vold[] = { "amixer", "-q", "set", "PCM", "5%-", "unmute", NULL };


#include "push.c"
static Key keys[] = {
  /* modifier               key               function        argument */
  { MODKEY,                 XK_p,             spawn,          {.v = menu } },
  { MODKEY,                 XK_r,             spawn,          {.v = dmenucmd } },
  { MODKEY,                 XK_F1,            spawn,          {.v = menu9cmd } },
  { MODKEY,                 XK_F2,            spawn,          {.v = menucmd } },
  { MODKEY,                 XK_w,             spawn,          {.v = webb } },
  { MODKEY,                 XK_s,             spawn,          {.v = chat } },
  { MODKEY,                 XK_Return,        spawn,          {.v = termcmd } },
  { MODKEY,                 XK_e,             spawn,          {.v = edit } },
  { MODKEY,                 XK_m,             spawn,          {.v = mdia } },
  { MODKEY,                 XK_f,             spawn,          {.v = file } },
  { MODKEY,                 XK_Print,         spawn,          {.v = prts } },
  { MODKEY,                 XK_F5,            spawn,          {.v = play } },
  { MODKEY,                 XK_F6,            spawn,          {.v = stop } },
  { MODKEY,                 XK_F7,            spawn,          {.v = prev } },
  { MODKEY,                 XK_F8,            spawn,          {.v = next } },
  { MODKEY,                 XK_F10,           spawn,          {.v = mute } },
  { MODKEY,                 XK_F11,           spawn,          {.v = vold } },
  { MODKEY,                 XK_F12,           spawn,          {.v = volu } },
  { MODKEY|ControlMask,     XK_b,             togglebar,      {0} },
  { MODKEY,                 XK_j,             focusstack,     {.i = +1 } },
  { MODKEY,                 XK_k,             focusstack,     {.i = -1 } },
  { MODKEY|ShiftMask,       XK_j,             pushdown,       {0} },
  { MODKEY|ShiftMask,       XK_k,             pushup,         {0} },
  { MODKEY,                 XK_i,             incnmaster,     {.i = +1 } },
  { MODKEY,                 XK_d,             incnmaster,     {.i = -1 } },
  { MODKEY,                 XK_h,             setmfact,       {.f = -0.05} },
  { MODKEY,                 XK_l,             setmfact,       {.f = +0.05} },
  { MODKEY,                 XK_Return,        zoom,           {0} },
  { MODKEY,                 XK_Tab,           view,           {0} },
  { MODKEY|ShiftMask,       XK_c,             killclient,     {0} },
  { MODKEY,                 XK_t,             setlayout,      {.v = &layouts[0]} },
  { MODKEY,                 XK_f,             setlayout,      {.v = &layouts[1]} },
  { MODKEY,                 XK_m,             setlayout,      {.v = &layouts[2]} },
  { MODKEY,                 XK_b,             setlayout,      {.v = &layouts[3]} },
  { MODKEY,                 XK_g,             setlayout,      {.v = &layouts[4]} },
  { MODKEY,                 XK_space,         setlayout,      {0} },
  { MODKEY|ShiftMask,       XK_space,         togglefloating, {0} },
  { MODKEY,                 XK_0,             view,           {.ui = ~0 } },
  { MODKEY|ShiftMask,       XK_0,             tag,            {.ui = ~0 } },
  { MODKEY,                 XK_comma,         focusmon,       {.i = -1 } },
  { MODKEY,                 XK_period,        focusmon,       {.i = +1 } },
  { MODKEY|ShiftMask,       XK_comma,         tagmon,         {.i = -1 } },
  { MODKEY|ShiftMask,       XK_period,        tagmon,         {.i = +1 } },
  TAGKEYS(                  XK_1,                             0)
  TAGKEYS(                  XK_2,                             1)
  TAGKEYS(                  XK_3,                             2)
  TAGKEYS(                  XK_4,                             3)
  TAGKEYS(                  XK_5,                             4)
  TAGKEYS(                  XK_6,                             5)
  TAGKEYS(                  XK_7,                             6)
  TAGKEYS(                  XK_8,                             7)
  TAGKEYS(                  XK_9,                             8)
  { MODKEY|ShiftMask,       XK_q,             quit,           {0} },
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click                event mask      button          function        argument */
  { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
  { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
  { ClkWinTitle,          0,              Button2,        zoom,           {0} },
  { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
  { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
  { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
  { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
  { ClkTagBar,            0,              Button1,        toggleview,     {0} },
  { ClkTagBar,            0,              Button3,        view,           {0} },
  { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
  { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

